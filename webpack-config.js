var path = require('path');
var LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
  entry: './src/index.js',
  devtool : 'cheap-source-map',
  output: {
    path: path.join(__dirname, 'dist/'),
    filename: 'bundle.js'
  },
  devServer:{
    contentBase: '.'
  },
  module: {
    loaders: [{
      test: /\.css$/,
      loader: 'style-loader!css-loader'
    },{
      test: /\.less$/,
      loader: 'style!css!less'
    },{
      test: /\.*\/templates\/.*.html/,
      loader: 'ngtemplate?requireAngular&relativeTo=src!html'
    },{
      test: /\.(png|jpg|gif)$/,
      loader: 'url-loader?limit=8192'
    },{
      test: /\.(woff|woff2|ttf|eot|svg)$/,
      loader: 'url?limit=10000'
    }]
  }
};
