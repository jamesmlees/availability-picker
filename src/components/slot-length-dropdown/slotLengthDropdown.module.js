module.exports = 'slot-length-dropdown';

angular.module(module.exports, [
])
  .directive('slotLengthDropdown', require('./slotLengthDropdown.directive.js'))
  .controller('slotLengthDropdownCtrl', require('./slotLengthDropdown.controller.js'))
;


require('./templates/slot-length-dropdown.html');
require('./styles/slot-length-dropdown.less');
