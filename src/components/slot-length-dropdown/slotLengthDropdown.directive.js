module.exports = function(){
  return {
    restrict: 'E',
    controller: 'slotLengthDropdownCtrl as ctrl',
    bindToController: true,
    templateUrl: '/components/slot-length-dropdown/templates/slot-length-dropdown.html',
    scope: {
      value: '=ngModel'
    },
  };
};

