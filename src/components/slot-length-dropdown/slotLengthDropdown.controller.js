module.exports = slotLengthDropdownController;

slotLengthDropdownController.$inject = [];
function slotLengthDropdownController(){
  var ctrl = this;

  var options = [{
    value: 1,
    display: '30 mins',
    minutes: 30,
  },{
    value: 2,
    display: '1hr',
    minutes: 60,
  },{
    value: 3,
    display: '2hr',
    minutes: 120,
  },{
    value: 4,
    display: '3hr',
    minutes: 180,
  }];

  ctrl.data = {
    options: options,
    selectedOption: options[0]
  };

  ctrl.update = update;

  /**
   * Sets value exposed by directive to minutes of the selected option
   */
  function update(){
    ctrl.value = ctrl.data.selectedOption.minutes;
  }
  return ctrl;
}


