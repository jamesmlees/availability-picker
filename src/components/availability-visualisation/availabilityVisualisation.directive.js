module.exports = function(){
  return {
    restrict: 'E',
    controller: 'AvailabilityVisualisationCtrl as ctrl',
    bindToController: true,
    templateUrl: '/components/availability-visualisation/templates/availability-visualisation.html',
    scope: {
      propertyId: '@'
    },
  };
};

