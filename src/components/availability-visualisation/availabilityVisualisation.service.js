module.exports = availabilityVisualisationService;


availabilityVisualisationService.$inject = ['lodash'];
function availabilityVisualisationService(_){
  var service = this;

  service.unsetSelected = unsetSelected;
  service.setSelected = setSelected;
  service.isSameAppointment = isSameAppointment;
  service.extractHumanReadableTime = extractHumanReadableTime;

  return service;

  /**
   * Deletes the selected key from every object in the provided array
   * @param {array} slots - array of timeslots
   */
  function unsetSelected(slots){
    _.each(slots, function(slot){
      delete slot.selected;
    });
  }

  /**
   * Appends a selected key to every object in the provided array
   * @param {array} slots - array of timeslots
   */
  function setSelected(slots){
    _.each(slots, function(block){
      _.extend(block, {selected: true});
    });
  }

  /**
   * Compares two appointment object
   * @param {object} newAppt - appointment for comparison
   * @param {object} oldAppt - appointment for comparison
   * @returns {boolean}
   */
  function isSameAppointment(apptA, apptB){
    if(apptA.day !== apptB.day) return false;
    if(apptA.timeSlots.length !== apptB.timeSlots.length) return false;
    return _.first(apptA.timeSlots).start === _.first(apptB.timeSlots).start;
  }

  /**
   * strips away the final :SS from HH:MM:SS format time
   * @param {string} time - format HH:MM:SS
   * @returns {string} format HH:MM
   */
  function extractHumanReadableTime(time){
    return time.split(':').slice(0,2).join(':');
  }


}
