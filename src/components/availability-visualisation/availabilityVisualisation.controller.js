module.exports = availabilityVisualisationController;

availabilityVisualisationController.$inject = ['lodash', 'apiHelper',
  'availabilityVisualisationSvc'];

function availabilityVisualisationController(_, apiHelper, service){
  var ctrl = this;
  ctrl.init = init;
  ctrl.selectTimeSlot = selectTimeSlot;
  ctrl.getTimeSlots = getTimeSlots;

  ctrl.init();

  /**
   * Returns the timeslots required to fulfil the user's requested appointment
   * duration based on the selected starting time slot
   * @param {object} day - object containing all booking information for a day
   * @param {array} day.startTimes - array of available timeslots
   * @param {string} day.day - date
   * @param {number} col - the column number selected, i.e. the start timeslot
   */
  function getTimeSlots(day, col){
    var minsRequired = ctrl.appointmentLength;
    var startSlot = day.startTimes[col];
    var mins = 0, i = 0;
    var slots = [];
    while(mins < minsRequired){
      let slot = day.startTimes[col + i];
      slots.push(slot);
      i++;
      mins += slot.duration;
    }
    return slots;
  }

  /**
   * Sets the selectedAppointment state on the controller, based on the user's
   * set duration and the slot clicked
   * @param {number} col - the column of the cell clicked by the user
   * @param {number} row - the row of the cell clicked by the user
   */
  function selectTimeSlot(col, row){
    var day = ctrl.data[row];
    var timeSlots = getTimeSlots(day, col);
    var newAppointment = {
      day: day.day,
      timeSlots: timeSlots
    };
    if(ctrl.selectedAppointment.day){
      service.unsetSelected(ctrl.selectedAppointment.timeSlots);
      var oldAppointment = _.cloneDeep(ctrl.selectedAppointment);
      ctrl.selectedAppointment = {};
      if(service.isSameAppointment(newAppointment, oldAppointment)){
        return;
      }
    }
    if(_.some(timeSlots, {possible: false})){
      return;
    }
    service.setSelected(timeSlots);
    ctrl.selectedAppointment = _.extend(newAppointment, {
      propertyId: ctrl.propertyId,
      startTime: _.first(timeSlots).start,
    });
  }

  /**
   * Extends availability data with additional information
   * @param {array} days - array of day objects
   * @param {object} days[n] - object containing all booking information for a day
   * @returns {array} the extended days array
   */
  function extendSlotData(days){
    return _.map(days, function(day){
      day.startTimes = _.map(day.startTimes, function(time){
        var startTime = new Date('2017-01-01T' + time.start);
        var endTime = new Date('2017-01-01T' + time.end);
        return _.extend(time, {
          duration: (endTime - startTime) / (1000 * 60),
          startDisplay: service.extractHumanReadableTime(time.start),
          endDisplay: service.extractHumanReadableTime(time.end)
        });
      });
      return day;
    });
  }

  /**
   * Initialises controller with necessary data
   * @returns {promise}
   */
  function init(){
    var apiPath = '/availability/?weekBeginning=2016-12-05&visitDuration=2.5&postcode=EC1R%203BU';
    ctrl.selectedAppointment = [];
    ctrl.appointmentLength = 30;
    return apiHelper.get(apiPath).then(function(data){
      ctrl.data = extendSlotData(data);
    }).catch(function(err){
      // TODO error handling
      console.error(err);
    });
  }
  return ctrl;
}


