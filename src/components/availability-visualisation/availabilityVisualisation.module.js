module.exports = 'availability-visualisation';

angular.module(module.exports, [
  require('../helpers'),
  require('../slot-length-dropdown'),
  require('../book-appointment')
])
  .value('lodash', require('lodash'))
  .directive('availabilityVisualisation', require('./availabilityVisualisation.directive.js'))
  .controller('AvailabilityVisualisationCtrl', require('./availabilityVisualisation.controller.js'))
  .service('availabilityVisualisationSvc', require('./availabilityVisualisation.service.js'))
;
require('./templates/availability-visualisation.html');
require('./styles/availability-visualisation.less');

