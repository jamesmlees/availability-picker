module.exports = 'book-appointment';

angular.module(module.exports, [
  'ui.bootstrap.modal',
  require('../helpers'),
])
  .directive('bookAppointment', require('./bookAppointment.directive.js'))
  .controller('bookAppointmentCtrl', require('./bookAppointment.controller.js'))
  .controller('bookAppointmentModalCtrl', require('./bookAppointmentModal.controller.js'))
;
require('./templates/book-appointment.html');
require('./templates/book-appointment-modal.html');
require('./styles/book-appointment.less');
