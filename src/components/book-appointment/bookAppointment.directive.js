module.exports = function(){
  return {
    restrict: 'E',
    controller: 'bookAppointmentCtrl as ctrl',
    bindToController: true,
    templateUrl: '/components/book-appointment/templates/book-appointment.html',
    scope: {
      appointment: '<',
      disabled: '=ngDisabled'
    },
  };
};

