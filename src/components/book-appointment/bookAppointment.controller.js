module.exports = bookAppointmentController;

bookAppointmentController.$inject = ['$uibModal', '$rootScope'];
function bookAppointmentController($modal, $rootScope){
  var ctrl = this;
  ctrl.openModal = openModal;

  function openModal(){
    var scope = $rootScope.$new();
    scope.appointment = ctrl.appointment;
    $modal.open({
      size: 'sm',
      templateUrl: '/components/book-appointment/templates/book-appointment-modal.html',
      controller: 'bookAppointmentModalCtrl',
      controllerAs: 'ctrl',
      bindToController: true,
      scope: scope,
    });
  }
  return ctrl;
}


