module.exports = bookAppointmentModalController;

bookAppointmentModalController.$inject = ['$uibModalInstance',
  'apiHelper', 'lodash'];

function bookAppointmentModalController(modalInstance, apiHelper, _){
  var ctrl = this;
  ctrl.confirmBooking = confirmBooking;
  ctrl.close = close;

  /**
   * Confirms booking
   * @returns {promise} api request
   */
  function confirmBooking(){
    ctrl.saving = true;
    var postObj = _.pick(ctrl.appointment, ['day', 'startTime', 'propertyId']);
    var duration = _.sumBy(ctrl.appointment.timeSlots, 'duration');
    postObj.duration = duration / 60;

    return apiHelper.post('/book', {
      data: _.omit(ctrl.appointment, 'timeSlots')
    }).then(function(data){
      ctrl.success = true;
      ctrl.saving = false;
      ctrl.cleaner = data.cleaner;
    }).catch(function(err){
      ctrl.error = err;
    }).finally(function(){
      ctrl.saving = false;
    });
  }

  function close(){
    modalInstance.close();
  }
  return ctrl;
}


