var sinon = require('sinon');
require('chai').should();
var $q = require('q');
var _ = require('lodash');

var factoryProvider = require('../apiHelper.factory.js');
describe('apiHelper factory', function(){
  var factory, mockHttp, opts, path;
  beforeEach(function(){
    mockHttp = sinon.stub().returns($q.when());
    factory = factoryProvider(mockHttp, $q, _);
    opts = {};
    path = 'some/path/to/resource';
  });
  it('should attach get and post methods', function(){
    factory.should.include.keys(['get', 'post']);
    factory.get.should.be.a('function');
    factory.post.should.be.a('function');
  });
  describe('get', function(){
    it('should wrap http get method', function(){
      return factory.get(path, opts).then(function(){
        mockHttp.callCount.should.be.eql(1);
        var expKeys = ['method', 'url', 'headers', 'params'];
        var arg = mockHttp.getCall(0).args[0];
        arg.should.be.an('object').with.keys(expKeys);
        arg.method.should.be.eql('GET');
      });
    });
    it('should append trailing slash', function(){
      return factory.get(path, opts).then(function(){
        mockHttp.callCount.should.be.eql(1);
        var arg = mockHttp.getCall(0).args[0];
        var regExp = new RegExp('.*' + path + '/$');
        arg.url.should.match(regExp);
      });
    });
    it('should include query params', function(){
      var params = opts.params = {test: 'some-query-params'};
      opts.params = params;
      return factory.get(path, opts).then(function(){
        mockHttp.getCall(0).args[0].params.should.be.eql(params);
      });
    });
    it('should return data', function(){
      var response = {data: {some: 'value'}};
      mockHttp.returns($q.when(response));
      return factory.get(path, opts).then(function(returnVal){
        returnVal.should.be.eql(response.data);
      });
    });
  });
  describe('post', function(){
    it('should wrap http post method', function(){
      return factory.post(path, opts).then(function(){
        mockHttp.callCount.should.be.eql(1);
        var expKeys = ['method', 'url', 'headers', 'params'];
        var arg = mockHttp.getCall(0).args[0];
        arg.should.be.an('object').with.keys(expKeys);
        arg.method.should.be.eql('POST');
      });
    });
    it('should include data', function(){
      var data = {some: 'data'};
      opts.data = data;
      return factory.post(path, opts).then(function(){
        mockHttp.callCount.should.be.eql(1);
        var arg = mockHttp.getCall(0).args[0];
        arg.should.be.an('object').and.include.key('data');
        arg.data.should.be.eql(data);
      });
    });
    it('should include query params', function(){
      var params = opts.params = {test: 'some-query-params'};
      opts.params = params;
      return factory.post(path, opts).then(function(){
        mockHttp.getCall(0).args[0].params.should.be.eql(params);
      });
    });
    it('should return data', function(){
      var response = {data: {some: 'value'}};
      mockHttp.returns($q.when(response));
      return factory.post(path, opts).then(function(returnVal){
        returnVal.should.be.eql(response.data);
      });
    });
  });
});
