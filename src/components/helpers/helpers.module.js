module.exports = 'helpers';

angular.module(module.exports, [])
  .factory('apiHelper', require('./apiHelper.factory'))
;
