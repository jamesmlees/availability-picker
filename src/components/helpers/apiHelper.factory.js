module.exports = apiHelperFactory;

apiHelperFactory.$inject = ['$http', '$q', 'lodash'];
function apiHelperFactory($http, $q, _){
  var factory = {};
  var baseUrl = 'https://private-anon-734d5f4b0a-housekeepavailability.apiary-mock.com';

  /**
   * Initialises the factory with helper functions
   */
  function init(){
    ['get', 'post'].forEach(function(verb){
      factory[verb] = function(path, opts){
        opts = opts || {};
        var req = {
          method: verb.toUpperCase(),
          url: baseUrl + path + '/',
          headers: {},
          params: opts.params || {},
        };
        if(opts.data) req.data = opts.data;
        return $http(req).then(function(res){
          return res && res.data;
        });
      };
    });
  }

  init();
  return factory;
}
