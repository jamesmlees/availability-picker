/* bundle entry point */

require('../node_modules/angular/angular.js');
require('../node_modules/lodash/lodash');
require('../node_modules/bootstrap/less/bootstrap.less');
require('./vendor/bootstrap/less/bootstrap.less');
require('../node_modules/angular-ui-bootstrap');


angular.module('housekeep',[
  'ui.bootstrap',
  require('./components/availability-visualisation'),
]);

